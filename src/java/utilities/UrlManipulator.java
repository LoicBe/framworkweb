/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author LoicRaveloarimanana
 */
public class UrlManipulator {

    private String[] splitSlasch(String url) {
        return url.split("/");
    }

    private String removeExtention(String url) {
        if (url.endsWith(".doo") == true)
            return url.substring(0, url.length() - ".doo".length());
        return "tsy misy extention";
    }

    public String retrieveUrlFromRawUrl(String url) {
        String[] field = splitSlasch(url);
        return removeExtention(field[field.length - 1]);
    }

    public String addExtention(String url) {
        return url + ".doo";
    }

}